﻿using System;
using ServiceStack.WebHost.Endpoints;

namespace SharpFit
{
    public class Global : System.Web.HttpApplication
    {
        public class HelloAppHost: AppHostBase
        {
            public HelloAppHost() : base("Hello Web Services", typeof (HelloService).Assembly) {}
            public override void Configure(Funq.Container container)
            {
                //register any dependencies your services use, e.g:
                //container.Register<ICacheClient>(new MemoryCacheClient());
                //throw new NotImplementedException();
            }
                
        }

        void Application_Start(object sender, EventArgs e)
        {
            try
            {
                var haHost = new HelloAppHost();
                haHost.Init();
                //new HelloAppHost().Init();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                throw ex;
            }

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
